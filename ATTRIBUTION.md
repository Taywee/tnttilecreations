Some of this code was taken or adapted from other sources.  They are given
credit in this file.

# zenlike

The CSS and some images were adapted from zenlike by nodethirtythree

> zenlike1.0 by nodethirtythree design
> http://www.nodethirtythree.com
>
> This template is released under the Creative Commons Attributions 2.5 license, which
> basically means you can do whatever you want with it provided you credit the author.
> (ie. me). In the case of this template, a link back to my site is more than sufficient.
>
> If you want to read the license, go here:
>
> http://creativecommons.org/licenses/by/2.5/
>
> If you're sufficiently intoxicated, or just like a good mental beating, you can also
> read the full legal code here:
>
> http://creativecommons.org/licenses/by/2.5/legalcode

# Background image

The background image is released under a CC-BY license via myfreetextures.com:
https://www.myfreetextures.com/generated-seamless-tile-background-texture-8/ 
