from django.db import models

from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel

class SectionPage(Page):
    '''A page for a section on the site.
    One of these should be the root of the site as well.
    '''

    body = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]
